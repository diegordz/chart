// /client/App.js
import React, { Component } from "react";
import axios from "axios";
// import ChartistGraph from "react-chartist";

// import React, { PureComponent } from "react";
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

class App extends Component {
  // constructor() {
  //   super();
  //   this.updateChart = this.updateChart.bind(this);
  // }
  // initialize our state
  state = {
    data: [],
    id: 0,
    message: null,
    intervalIsSet: false,
    idToDelete: null,
    idToUpdate: null,
    objectToUpdate: null
  };

  // when component mounts, first thing it does is fetch all existing data in our db
  // then we incorporate a polling logic so that we can easily see if our db has
  // changed and implement those changes into our UI
  componentDidMount() {
    this.getDataFromDb();
    if (!this.state.intervalIsSet) {
      let interval = setInterval(this.getDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
  }

  // never let a process live forever
  // always kill a process everytime we are done using it
  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
  }

  // just a note, here, in the front end, we use the id key of our data object
  // in order to identify which we want to Update or delete.
  // for our back end, we use the object id assigned by MongoDB to modify
  // data base entries

  // our first get method that uses our backend api to
  // fetch data from our data base
  getDataFromDb = () => {
    fetch("http://localhost:3001/api/getData")
      .then(data => data.json())
      .then(res => this.setState({ data: res.data }));
  };

  // our put method that uses our backend api
  // to create new query into our data base

  // our delete method that uses our backend api
  // to remove existing database information
  deleteFromDB = idTodelete => {
    let objIdToDelete = null;
    this.state.data.forEach(dat => {
      if (dat.id === idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete("http://localhost:3001/api/deleteData", {
      data: {
        id: objIdToDelete
      }
    });
  };

  putDataToDB = (message, amount) => {
    let currentIds = this.state.data.map(data => data.id);
    let idToBeAdded = 0;
    while (currentIds.includes(idToBeAdded)) {
      ++idToBeAdded;
    }

    axios.post("http://localhost:3001/api/putData", {
      id: idToBeAdded,
      message: message,
      amount: amount
    });
  };

  // our update method that uses our backend api
  // to overwrite existing data base information
  updateDB = (idToUpdate, updateToApply) => {
    let objIdToUpdate = null;
    this.state.data.forEach(dat => {
      if (dat.id === idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    axios.post("http://localhost:3001/api/updateData", {
      id: objIdToUpdate,
      update: { message: updateToApply }
    });
  };

  addToArray = () => {
    const xarray = [];
    const test = this.state.data.map((skills, i) => skills.message);
    const numtest = this.state.data.map((skills, i) => skills.amount);
    for (var i = 0; i < this.state.data.length; i++) {
      xarray[i] = { name: test[i], Skills: numtest[i] };
    }
    return xarray;
  };

  render() {
    const { data } = this.state;

    const test = data.map((skills, i) => skills.message);
    const numtest = data.map((skills, i) => skills.id);

    // const testarray = data.map(skills => {
    //   skills.message, skills.id;
    // });

    // console.log(this.addToArray());
    // console.log(test);
    // const skill = {
    //   labels: [test[0], test[1]],
    //   labels: [this.addToArray()],
    //   series: [[1, 2]]
    // };

    // var options = {
    //   high: 10,
    //   low: -10
    //   // axisX: {
    //   //   labelInterpolationFnc: function(value, index) {
    //   //     return index % 2 === 0 ? value : null;
    //   //   }
    //   // }
    // };

    // var type = "Bar";

    const chartdata = this.addToArray();
    return (
      <div>
        <ul>
          {data.length <= 0
            ? "NO DB ENTRIES YET"
            : data.map(dat => (
                <li style={{ padding: "10px" }} key={dat.message}>
                  <span style={{ color: "gray" }}> id: </span> {dat.id} <br />
                  <span style={{ color: "gray" }}> data: </span>
                  {dat.message} <br />
                  <span style={{ color: "gray" }}> amount: </span>
                  {dat.amount} <br />
                </li>
              ))}
        </ul>
        <div style={{ padding: "10px" }}>
          <input
            type="text"
            onChange={e => this.setState({ message: e.target.value })}
            placeholder="add something in the database"
            style={{ width: "200px" }}
          />
          <input
            type="text"
            onChange={e => this.setState({ amount: e.target.value })}
            placeholder="amount"
            style={{ width: "200px" }}
          />
          <button
            onClick={() =>
              this.putDataToDB(this.state.message, this.state.amount)
            }
          >
            ADD
          </button>
        </div>
        <div style={{ padding: "10px" }}>
          <input
            type="text"
            style={{ width: "200px" }}
            onChange={e => this.setState({ idToDelete: e.target.value })}
            placeholder="put id of item to delete here"
          />
          <button onClick={() => this.deleteFromDB(this.state.idToDelete)}>
            DELETE
          </button>
        </div>
        <div style={{ padding: "10px" }}>
          <input
            type="text"
            style={{ width: "200px" }}
            onChange={e => this.setState({ idToUpdate: e.target.value })}
            placeholder="id of item to update here"
          />
          <input
            type="text"
            style={{ width: "200px" }}
            onChange={e => this.setState({ updateToApply: e.target.value })}
            placeholder="put new value of the item here"
          />
          <button
            onClick={() =>
              this.updateDB(this.state.idToUpdate, this.state.updateToApply)
            }
          >
            UPDATssE
          </button>
        </div>
        <div>
          {/* <ChartistGraph data={skill} options={options} type={type} /> */}

          <BarChart
            width={500}
            height={300}
            data={chartdata}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />

            <Bar dataKey="Skills" fill="#82ca9d" />
          </BarChart>
        </div>
      </div>
    );
  }
}

export default App;
